package com.example.archetype.modernizacaoservicearchetype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModernizacaoServiceArchetypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModernizacaoServiceArchetypeApplication.class, args);
	}
}
